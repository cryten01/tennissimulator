public enum UIState
{
    StartGame,
    OnGoing,
    PlayerWon,
    OpponentWon,
}