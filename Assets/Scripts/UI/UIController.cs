using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField] TextMeshPro playerPointScoreText;
    [SerializeField] TextMeshPro playerGameScoreText;
    [SerializeField] TextMeshPro playerSetScoreText;

    [SerializeField] TextMeshPro opponentPointScoreText;
    [SerializeField] TextMeshPro opponentGameScoreText;
    [SerializeField] TextMeshPro opponentSetScoreText;

    [SerializeField] Button controlButton;
    [SerializeField] TextMeshProUGUI controlButtonText;

    [SerializeField] TextMeshProUGUI gameStatusText;

    public void ChangeUIState(UIState state)
    {
        switch (state)
        {
            case UIState.StartGame:
                SetGameStatusText("Press start to run the simulation.");
                SetButtonText("Start");
                break;
            case UIState.OnGoing:
                ResetAllScoreTexts();
                SetGameStatusText("Ongoing match");
                SetButtonText("Restart");
                break;
            case UIState.PlayerWon:
                SetGameStatusText("Congrats you have won the game!");
                SetButtonText("Restart");
                break;
            case UIState.OpponentWon:
                SetGameStatusText("Sorry but your opponent kicked your ass!");
                SetButtonText("Restart");
                break;
        }
    }

    public void SetPlayerPointScoreText(string score)
    {
        playerPointScoreText.text = score;
    }

    public void SetPlayerGameScoreText(int score)
    {
        playerGameScoreText.text = score.ToString();
    }

    public void SetPlayerSetScoreText(int score)
    {
        playerSetScoreText.text = score.ToString();
    }

    public void SetOpponentPointScoreText(string score)
    {
        opponentPointScoreText.text = score;
    }

    public void SetOpponentGameScoreText(int score)
    {
        opponentGameScoreText.text = score.ToString();
    }

    public void SetOpponentSetScoreText(int score)
    {
        opponentSetScoreText.text = score.ToString();
    }

    void ResetAllScoreTexts()
    {
        playerPointScoreText.text = "0";
        playerGameScoreText.text = "0";
        playerSetScoreText.text = "0";

        opponentPointScoreText.text = "0";
        opponentGameScoreText.text = "0";
        opponentSetScoreText.text = "0";
    }

    void SetGameStatusText(string text)
    {
        gameStatusText.text = text;
    }

    void SetButtonText(string text)
    {
        controlButtonText.text = text.ToUpper();
    }
}
