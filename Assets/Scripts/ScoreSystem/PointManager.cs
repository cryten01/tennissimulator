using System.Collections;
using UnityEngine;

public class PointManager : MonoBehaviour
{
    [SerializeField] float timeBetweenPoints = 1.0f;

    int playerPointScore;
    int opponentPointScore;
    UIController uiController;

    void Awake()
    {
        uiController = FindObjectOfType<UIController>();
    }

    public void ResetPointScore()
    {
        playerPointScore = 0;
        opponentPointScore = 0;
    }

    public IEnumerator PlayPoint()
    {
        GameState finalPointState = DetermineRandomPointWinner();
        UpdateScore(finalPointState);
        yield return new WaitForSeconds(timeBetweenPoints);
    }

    public GameState CheckIfRoundWinner()
    {
        if (playerPointScore >= 4 && (playerPointScore - opponentPointScore) >= 2)
        {
            return GameState.PlayerWins;
        }
        else if (opponentPointScore >= 4 && (opponentPointScore - playerPointScore) >= 2)
        {
            return GameState.OpponentWins;
        }
        else
        {
            return GameState.Ongoing;
        }
    }

    GameState DetermineRandomPointWinner()
    {
        float playerRandom = 0f;
        float opponentRandom = 0f;

        while (playerRandom == opponentRandom)
        {
            playerRandom = UnityEngine.Random.Range(0f, 1f);
            opponentRandom = UnityEngine.Random.Range(0f, 1f);
        }

        return playerRandom > opponentRandom ? GameState.PlayerWins : GameState.OpponentWins;
    }

    void UpdateScore(GameState state)
    {
        if (state == GameState.PlayerWins)
        {
            playerPointScore++;
        }
        else if (state == GameState.OpponentWins)
        {
            opponentPointScore++;
        }

        SetUIScore();
    }

    void SetUIScore()
    {
        string playerScoreText = ConvertPointScoreToText(playerPointScore, opponentPointScore);
        string opponentScoreText = ConvertPointScoreToText(opponentPointScore, playerPointScore);

        uiController.SetPlayerPointScoreText(playerScoreText);
        uiController.SetOpponentPointScoreText(opponentScoreText);
    }

    string ConvertPointScoreToText(int score, int otherScore)
    {
        if (score == 1)
        {
            return "15";
        }
        else if (score == 2)
        {
            return "30";
        }
        else if (score == 3)
        {
            return "40";
        }
        else if (score >= 4)
        {
            if ((score - otherScore) >= 2)
            {
                return "Game";
            }
            else if (score > otherScore)
            {
                return "Ad";
            }
            else
            {
                return "40";
            }
        }
        else
        {
            return "Love";
        }
    }
}