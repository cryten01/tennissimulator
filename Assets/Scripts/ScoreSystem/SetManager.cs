using System.Collections;
using UnityEngine;

public class SetManager : MonoBehaviour
{
    [SerializeField] float timeBetweenSets = 1.0f;

    int playerSetScore;
    int opponentSetScore;
    UIController uiController;
    RoundManager roundManager;

    void Awake()
    {
        uiController = FindObjectOfType<UIController>();
        roundManager = GetComponent<RoundManager>();
    }

    public void ResetSetScore()
    {
        playerSetScore = 0;
        opponentSetScore = 0;
    }

    public IEnumerator PlaySet()
    {
        roundManager.ResetRoundScore();

        while (roundManager.CheckIfSetWinner() == GameState.Ongoing)
        {
            yield return StartCoroutine(roundManager.PlayRound());
        }

        GameState finalRoundState = roundManager.CheckIfSetWinner();
        UpdateScore(finalRoundState);

        yield return new WaitForSeconds(timeBetweenSets);
    }

    public GameState CheckIfMatchWinner()
    {
        if (playerSetScore >= 2)
        {
            return GameState.PlayerWins;
        }
        else if (opponentSetScore >= 2)
        {
            return GameState.OpponentWins;
        }
        else
        {
            return GameState.Ongoing;
        }
    }

    void UpdateScore(GameState state)
    {
        if (state == GameState.PlayerWins)
        {
            playerSetScore++;
        }
        else if (state == GameState.OpponentWins)
        {
            opponentSetScore++;
        }

        SetUIScore();
    }

    void SetUIScore()
    {
        uiController.SetPlayerSetScoreText(playerSetScore);
        uiController.SetOpponentSetScoreText(opponentSetScore);
    }

}