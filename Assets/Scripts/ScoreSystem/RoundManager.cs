using System.Collections;
using UnityEngine;

public class RoundManager : MonoBehaviour
{
    [SerializeField] float timeBetweenRounds = 1.0f;

    int playerRoundScore;
    int opponentRoundScore;
    PointManager pointManager;
    UIController uiController;

    void Awake()
    {
        pointManager = GetComponent<PointManager>();
        uiController = FindObjectOfType<UIController>();
    }

    public void ResetRoundScore()
    {
        playerRoundScore = 0;
        opponentRoundScore = 0;
    }

    public IEnumerator PlayRound()
    {
        pointManager.ResetPointScore();

        while (pointManager.CheckIfRoundWinner() == GameState.Ongoing)
        {
            yield return StartCoroutine(pointManager.PlayPoint());
        }

        GameState finalRoundState = pointManager.CheckIfRoundWinner();
        UpdateScore(finalRoundState);

        yield return new WaitForSeconds(timeBetweenRounds);
    }

    public GameState CheckIfSetWinner()
    {
        if (playerRoundScore > 6)
        {
            return GameState.PlayerWins;
        }
        else if (opponentRoundScore > 6)
        {
            return GameState.OpponentWins;
        }
        else
        {
            return GameState.Ongoing;
        }
    }

    void UpdateScore(GameState state)
    {
        if (state == GameState.PlayerWins)
        {
            playerRoundScore++;
        }
        else if (state == GameState.OpponentWins)
        {
            opponentRoundScore++;
        }

        SetUIScore();
    }

    void SetUIScore()
    {
        uiController.SetPlayerGameScoreText(playerRoundScore);
        uiController.SetOpponentGameScoreText(opponentRoundScore);
    }
}