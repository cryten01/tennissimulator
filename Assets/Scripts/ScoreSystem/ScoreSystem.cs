using UnityEngine;

public class ScoreSystem : MonoBehaviour
{
    UIController uiController;
    MatchManager matchManager;

    void Awake()
    {
        uiController = FindObjectOfType<UIController>();
        matchManager = GetComponent<MatchManager>();
        uiController.ChangeUIState(UIState.StartGame);
    }

    public void StartSimulation()
    {
        uiController.ChangeUIState(UIState.OnGoing);
        StartCoroutine(matchManager.PlayMatch());
    }
}
