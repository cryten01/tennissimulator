public enum GameState
{
    PlayerWins,
    OpponentWins,
    Ongoing,
}