using System.Collections;
using UnityEngine;

public class MatchManager : MonoBehaviour
{
    [SerializeField] float timeBetweenMatches = 1.0f;

    SetManager setManager;
    UIController uiController;

    void Awake()
    {
        setManager = GetComponent<SetManager>();
        uiController = FindObjectOfType<UIController>();
    }

    public IEnumerator PlayMatch()
    {
        setManager.ResetSetScore();

        while (setManager.CheckIfMatchWinner() == GameState.Ongoing)
        {
            yield return StartCoroutine(setManager.PlaySet());
        }

        GameState finalRoundState = setManager.CheckIfMatchWinner();
        UpdateScore(finalRoundState);

        yield return new WaitForSeconds(timeBetweenMatches);
    }

    void UpdateScore(GameState state)
    {
        if (state == GameState.PlayerWins)
        {
            uiController.ChangeUIState(UIState.PlayerWon);
        }
        else if (state == GameState.OpponentWins)
        {
            uiController.ChangeUIState(UIState.OpponentWon);
        }
    }
}